import { makeVar } from '@apollo/client';
import { InMemoryCache } from '@apollo/client';

const currentPokemons = JSON.parse(localStorage.getItem('myPokemon'))
export let myPokemon
if (currentPokemons) {
    myPokemon = makeVar(currentPokemons);
} else {
    myPokemon = makeVar([]);
}
export const pageLoad = makeVar(8)

export const limitLoad = pageLoad() - makeVar(8)
export const offsetLoad = makeVar(8)

export const cache = new InMemoryCache()
export const getLoadPokemon = () => {
    return pageLoad()
}

export const loadMorePokemon = () => {
    pageLoad(pageLoad() + 3)
}
// export default cache