import React, { useState } from 'react';
import { useQuery } from '@apollo/client';
import querie from '../graph/querie';
import CardList from '../components/CardList';
import './PokemonList.css';
import Loading from '../components/Loading';
import Error from '../components/Error';

const PokemonList = () => {
    const [limitState, setLimitState] = useState(JSON.parse(localStorage.getItem("offset")))
    const variables = { limit: 8 , offset: JSON.parse(localStorage.getItem("offset"))-8}
    const {loading, error, data} = useQuery(querie.GET_POKEMON_LIST, { variables })

    const handleMore = () => {
        localStorage.setItem("offset",JSON.parse(localStorage.getItem("offset"))+8)
        setLimitState(limitState + 8)

    }
    // const handleLess = () => {
    //     if (limitState - 3 <= 0) {
    //         setLessWarningState(true)
    //         setTimeout(()=>{
    //             setLessWarningState(false)
    //         },2000)
    //     } else {
    //         loadLessPokemon()
    //         setLimitState(pageLoad())
    //     }
    // }

    const handleLess = () => {
        if(limitState > 8){
            localStorage.setItem("offset",JSON.parse(localStorage.getItem("offset"))-8)
            setLimitState(limitState - 8)
        }
        

    }

    if (loading) return <Loading msg='getting pokemon list...' />
    if (error) return <Error />
    if (data) {
        const pokemonList = data.pokemons.results.map((pokemon) => {
            let catchedPokemon = querie.getMyPokemon().filter(({name}) => {
                return pokemon.name === name
            })
            return Object.assign({...pokemon}, {catched: catchedPokemon.length})
        })

        const pokemons = pokemonList.map(({name, image, catched, id}) => (
            <CardList 
                key={name}
                name={name}
                image={image}
                catched={catched}
                id={id}
            />
        ))

        return (
            <div className="list">
                <h2 className="title-pokemont-list">Gotta Catch 'Em All</h2>
                <div>
                    <div className="list-layout-pokemont-list">
                        {pokemons}
                    </div>
                    <div className="page-control-layout">
                        <div className="page-control-box">
                            <div className="button-style">
                                <button onClick={handleLess} className="Button">Previous</button>
                                <button  onClick={handleMore} className="Button">Next</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default PokemonList;