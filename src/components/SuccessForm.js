import React, { useState } from 'react';
import './SuccessForm.css';
import querie from '../graph/querie';
import { useHistory } from 'react-router-dom';
import uuid from 'react-uuid'
import Mutation from '../graph/mutation';
import MyPokemon from '../pages/MyPokemon'


const SuccessForm = (props) => {
    const imageUrl = `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${props.pokemon.id}.png`
    const history = useHistory()
    const [ existState, setExistState ] = useState(false)
    const [ editState, setEditState ] = useState(false)
    const unique = uuid()

    const checkExist = (given_name) => {
        const listMyPokemon = querie.getMyPokemon();
        const check = listMyPokemon.filter((pokemon) => {
            return pokemon.given_name === given_name
        })
        if (check.length) {
            return false
        } else {
            return true
        }
    }

    const handleSubmit = (e) => {
        e.preventDefault()
        const newPokemon = {
            id: props.pokemon.id,
            given_name: e.target.given_name.value,
            name: props.pokemon.name,
            unique: unique
        }
        if(props.pokemon.unique){
            Mutation.editPokemon(props.pokemon.unique,e.target.given_name.value)
            setEditState(true)
        } else{
            if(checkExist(e.target.given_name.value ) && e.target.given_name.value){
                Mutation.createPokemon(newPokemon)
                setExistState(false)
                history.push('/mypocket')
            }
            else{
                setExistState(true)
            }
        }
        
    }

    if(editState){
        return (<MyPokemon />)
    }

    return (
        <div className="form-layout">
            <div className="content">
                <h3>Gotcha! {props.pokemon.name} catched</h3>
                <img src={imageUrl} alt="pokemon" />
                <p>Give a Name:</p>
                <form autoComplete="off" onSubmit={handleSubmit}>
                    <input className="input-name" type="text" name="given_name"></input>
                    <button className="submit-button Button" type="submit" value="ok">Submit</button>
                </form>
                {
                    existState
                    ? <code className="red-text">given_name already exist or the field still empty</code>
                    : <span></span>
                }
            </div>
        </div>
    )
}

export default SuccessForm