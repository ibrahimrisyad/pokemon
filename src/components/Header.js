import React from 'react';
import './Header.css';

const Header = () => {
    return (
        <header>
            <div className="title-container">
                <img src="https://images.wikidexcdn.net/mwuploads/esssbwiki/thumb/7/77/latest/20111028181540/TituloUniversoPok%C3%A9mon.png/550px-TituloUniversoPok%C3%A9mon.png" alt="header"/>
            </div>
        </header>
    )
}

export default Header;