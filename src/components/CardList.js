import React from 'react';
import './CardList.css';
import { useHistory } from 'react-router-dom';

const CardList = (props) => {
    const history = useHistory()
    const mini = true
    const handleClick = () => {
        history.push(`/pokemon/${props.name}`)
    }

    return (

<div
className={`pokemon-card ${mini && 'pokemon-card-mini'}`}
onClick={handleClick}
>
<h1>#{props.id}</h1>

<img src={props.image} alt='pokemon' />
<h2>{props.name}</h2>
{
    props.given_name
    ? <p>Given Name: {props.given_name}</p>
    : <p>Catched: {props.catched}</p>
}
{
    props.given_name
    ? <div style={{display:'flex'}}> 
        <button onClick={(event)=>props.onRelease(event, props.given_name)} className="Button">Release</button>
        <button onClick={(event)=>props.onRename(event, props)} className="Button">Rename</button>
     </div>
    : <span />
}
{!mini && (
  <>
    <p className="classification">{props.given_name}</p>
    <div className="type-container">
      {props.given_name}
    </div>
  </>
)}
</div>
    )
}

export default CardList