import React from 'react';
import './Empty.css';

const Empty = () => {
    return (
        <div className="empty">
            <div>
                <img src="https://i.pinimg.com/564x/a0/36/74/a03674ba07d318077a4604780d085dfe.jpg" alt="supreise_pikachu"></img>
                <h4>Oh no! Your pocket is empty. Catch some Pokémon and add them to your pocket by clicking catch button on the Pokemon List page!</h4>
            </div>
        </div>
    )
}

export default Empty